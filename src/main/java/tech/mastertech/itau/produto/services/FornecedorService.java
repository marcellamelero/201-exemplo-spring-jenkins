package tech.mastertech.itau.produto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.produto.models.Fornecedor;
import tech.mastertech.itau.produto.repositories.FornecedorRepository;

@Service
public class FornecedorService {
  @Autowired
  private FornecedorRepository fornecedorRepository;
  
  public Fornecedor setFornecedor(Fornecedor fornecedor) {
    return fornecedorRepository.save(fornecedor);
  }
  
  public Iterable<Fornecedor> getFornecedores(){
    return fornecedorRepository.findAll();
  }
}
