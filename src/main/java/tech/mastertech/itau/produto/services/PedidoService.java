package tech.mastertech.itau.produto.services;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.produto.models.Pedido;
import tech.mastertech.itau.produto.repositories.PedidoRepository;

@Service
public class PedidoService {
  @Autowired
  private PedidoRepository pedidoRepository;
  
  public Pedido setPedido(Pedido categoria) {
    categoria.setData(LocalDate.now());
    return pedidoRepository.save(categoria);
  }
  
  public Iterable<Pedido> getPedidos(){
    return pedidoRepository.findAll();
  }
}
