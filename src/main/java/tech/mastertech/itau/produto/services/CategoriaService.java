package tech.mastertech.itau.produto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.repositories.CategoriaRepository;

@Service
public class CategoriaService {
  @Autowired
  private CategoriaRepository categoriaRepository;
  
  public Categoria setCategoria(Categoria categoria) {
    return categoriaRepository.save(categoria);
  }
  
  public Iterable<Categoria> getCategorias(){
    return categoriaRepository.findAll();
  }
}
