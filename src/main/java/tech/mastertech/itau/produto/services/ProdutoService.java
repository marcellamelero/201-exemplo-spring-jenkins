package tech.mastertech.itau.produto.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.models.Produto;
import tech.mastertech.itau.produto.repositories.ProdutoRepository;

@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Iterable<Produto> getProdutos() {		
		return produtoRepository.findAll();
	}
	
	public Iterable<Produto> getProdutos(Categoria categoria){
		return produtoRepository.findAllByCategoria(categoria);
	}
	
	public Produto getProduto(int idProduto) {
		Optional<Produto> retorno = produtoRepository.findById(idProduto);
		
		if(!retorno.isPresent()) {
          throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		
		return retorno.get();
	}

	public Produto setProduto(Produto produto) {
		return produtoRepository.save(produto);
	}

	public Produto mudarValor(int idProduto, double valor) {
		if(valor <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Valor inválido - deve ser um positivo");
		}
		
		Produto produto = getProduto(idProduto);
		produto.setValor(valor);
		return produtoRepository.save(produto);
	}
}
