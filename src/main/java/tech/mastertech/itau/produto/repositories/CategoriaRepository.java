package tech.mastertech.itau.produto.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.produto.models.Categoria;

public interface CategoriaRepository extends CrudRepository<Categoria, Integer> {
}
