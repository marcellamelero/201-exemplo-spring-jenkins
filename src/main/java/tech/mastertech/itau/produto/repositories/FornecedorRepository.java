package tech.mastertech.itau.produto.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.produto.models.Fornecedor;

public interface FornecedorRepository extends CrudRepository<Fornecedor, Integer> {
}
