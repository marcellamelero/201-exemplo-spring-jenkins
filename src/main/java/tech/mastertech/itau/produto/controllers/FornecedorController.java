package tech.mastertech.itau.produto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.produto.models.Fornecedor;
import tech.mastertech.itau.produto.services.FornecedorService;

@RestController
public class FornecedorController {
  
  @Autowired
  private FornecedorService fornecedorService;

  @PostMapping("/fornecedor")
  public Fornecedor setCategoria(@RequestBody Fornecedor fornecedor) {
    return fornecedorService.setFornecedor(fornecedor);
  }
  
  @GetMapping("/fornecedores")
  public Iterable<Fornecedor> getFornecedores(){
    return fornecedorService.getFornecedores();
  }
  
}
