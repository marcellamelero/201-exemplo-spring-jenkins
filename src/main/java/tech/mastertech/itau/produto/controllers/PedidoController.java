package tech.mastertech.itau.produto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.produto.models.Pedido;
import tech.mastertech.itau.produto.services.PedidoService;

@RestController
public class PedidoController {

  @Autowired
  private PedidoService pedidoService;

  @PostMapping("/pedido")
  public Pedido setCategoria(@RequestBody Pedido pedido) {
    return pedidoService.setPedido(pedido);
  }
  
  @GetMapping("/pedidos")
  public Iterable<Pedido> getPedidos(){
    return pedidoService.getPedidos();
  }
  
}
