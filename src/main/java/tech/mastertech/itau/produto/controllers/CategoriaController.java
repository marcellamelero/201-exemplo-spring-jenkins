package tech.mastertech.itau.produto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.services.CategoriaService;

@RestController
public class CategoriaController {
  
  @Autowired
  private CategoriaService categoriaService;

  @PostMapping("/categoria")
  public Categoria setCategoria(@RequestBody Categoria categoria) {
    return categoriaService.setCategoria(categoria);
  }
  
  @GetMapping("/categorias")
  public Iterable<Categoria> getCategorias(){
    return categoriaService.getCategorias();
  }
  
}
