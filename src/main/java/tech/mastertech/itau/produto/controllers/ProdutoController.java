package tech.mastertech.itau.produto.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.models.Produto;
import tech.mastertech.itau.produto.services.ProdutoService;

@RestController
public class ProdutoController {
  
	@Autowired
	private ProdutoService produtoService;
	
	@GetMapping("/produtos")
	public Iterable<Produto> getProdutos(@RequestParam(required=false) Categoria categoria){
		if(categoria == null) {
			return produtoService.getProdutos();	
		}
		return produtoService.getProdutos(categoria);
	}
	
	@GetMapping("/produto/{idProduto}")
	public Produto getProduto(@PathVariable int idProduto) {
		return produtoService.getProduto(idProduto);
	}
	
	@PostMapping("/produto")
	public Produto setProduto(@RequestBody Produto produto) {
		return produtoService.setProduto(produto);
	}
	
	@PatchMapping("/produto/{idProduto}/valor")
	public Produto mudarValor(@PathVariable int idProduto, @RequestBody Map<String, Double> valor) {
		return produtoService.mudarValor(idProduto, valor.get("valor"));
	}
	
}
